WHAT IT IS?
Chrome extension, that generates phpBB code out of your steam screenshots. Just go to your steamcommunity.com page, select screenshots with checkboxes in upper-rights and click "copy" in extension window.

download this zip: https://bitbucket.org/mcyraniak/steamscreenshots/downloads/steamscreenshots.zip

TO INSTALL:
![Install.png](https://bitbucket.org/repo/5e5KyK/images/460758463-Install.png)

TO USE:
![Use.png](https://bitbucket.org/repo/5e5KyK/images/712714798-Use.png)