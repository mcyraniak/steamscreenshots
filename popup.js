// function button_add_images(e) {
//     chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
//         chrome.tabs.sendMessage(tabs[0].id, {status: 'ok'}, function(response) {
//             console.log(response);
//         });
//     });
// }

function button_show_checkboxes() {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {command: 'show_checkboxes'}, function(response) {
            if (response['status'] == 'ok') {
                $('#btn_show_checkboxes').hide();
                $('#btn_hide_checkboxes').show();
                $('#btn_copy_code').removeAttr('disabled');
            } else {
                console.log(response['status']);
            }
        });
    });
}

function button_hide_checkboxes() {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {command: 'hide_checkboxes'}, function(response) {
            if (response['status'] == 'ok') {
                $('#btn_show_checkboxes').show();
                $('#btn_hide_checkboxes').hide();
                $('#btn_copy_code').attr('disabled', 'disabled');
            } else {
                console.log(response['status']);
            }
        });
    });
}

function button_uncheck_all() {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {command: 'uncheck_all'}, function(response) {
            if (response['status'] != 'ok') {
                console.log(response['status']);
            }
        });
    });
}

function generate_code(src) {
    var resize = '320x200.resizedimage';
    return '[url=' + src + '][img]' + src + resize + '[/img][/url]';
}

function copy_to_clipboard(text) {
    chrome.runtime.sendMessage({command: 'copy_to_clipboard', text: text});
}

function button_copy_code() {
    var full_code = '';

    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {command: 'gather_data'}, function(response) {
            if (response['status'] == 'ok') {
                var data = response['data'];
                if (data) {
                    for (var x=0; x<data.length; x++) {
                        var src = data[x];
                        var code = generate_code(src);
                        full_code += code;
                    }
                }
                $('#code').text(full_code);
                copy_to_clipboard(full_code);
            } else {
                console.log(response['status']);
            }
        });
    });
}

document.addEventListener('DOMContentLoaded', function () {
    $('#btn_show_checkboxes').click(function(e) { button_show_checkboxes(); });
    $('#btn_hide_checkboxes').click(function(e) { button_hide_checkboxes(); });
    $('#btn_uncheck_all').click(function(e) { button_uncheck_all(); });
    $('#btn_copy_code').click(function(e) { button_copy_code(); });
});