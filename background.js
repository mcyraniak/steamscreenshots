chrome.runtime.onInstalled.addListener(function() {
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [
                new chrome.declarativeContent.PageStateMatcher({
                    pageUrl: {
                        hostEquals: 'steamcommunity.com'
                    }
                })
            ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });
});

chrome.runtime.onMessage.addListener(
    function(msg, sender, sendResponse) {
        if (msg['command'] == 'copy_to_clipboard') {
            var textarea = document.getElementById("tmp-clipboard");
            // now we put the message in the textarea
            textarea.value = msg.text;
            // and copy the text from the textarea
            textarea.select();
            document.execCommand("copy", false, null);
            // finally, cleanup / close the connection
            sendResponse({});
        }
    }
);