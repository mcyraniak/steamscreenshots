var previousNumberOfInfiniteElements = 0;

function checkIfItsScreenshotsTab() {
    var tabs = document.getElementsByClassName("sectionTabs item")[0];
    if (tabs) {
        return tabs.children[0] && (tabs.children[0].className == 'sectionTab  active' || tabs.children[0].className == 'sectionTab active');
    } else return false;
}

function checkNumberOfInfiniteElements() {
    var infiniteScrollingContainer = checkIfItsScreenshotsTab() && document.getElementById("InfiniteScrollingContainer");
    if (infiniteScrollingContainer) {
        var num = infiniteScrollingContainer.children.length;
        if (num != previousNumberOfInfiniteElements) {
            previousNumberOfInfiniteElements = num;
            insertNewCheckboxes();
        }
    }

    setTimeout(checkNumberOfInfiniteElements, 100);
}

function insertNewCheckboxes() {
    wallItems = document.getElementsByClassName("imgWallItem");
    for (var x = 0; x < wallItems.length; x++) {
        var item = wallItems[x];
        var imgWallHover = item.getElementsByClassName("imgWallHover")[0];
        if (imgWallHover) {
            var oldCheckbox = imgWallHover.getElementsByClassName("boberro_steamscreenshots_checkbox")[0];
            if (!oldCheckbox) {
                var newCheckbox = document.createElement("input");
                newCheckbox.setAttribute("type", "checkbox");
                newCheckbox.className = "boberro_steamscreenshots_checkbox";
                newCheckbox.onclick = function(e) {
                    e.stopPropagation(); //stop steam functions mess with mah new button
                };
                imgWallHover.appendChild(newCheckbox);
            }
        }
    }
}

function gatherData() {
    reg = new RegExp("[0-9]+x[0-9]+.resizedimage");

    var data = [];
    $('input.boberro_steamscreenshots_checkbox:checked').closest('div.imgWallItem').find('> img').each(function(index) {
        var src = this.src;
        if (src) {
            resize = reg.exec(src);
            if (resize) {
                src = src.slice(0, src.indexOf(resize));
            }
            data.push(src);
        }
    });
    return data;
}

//extension events

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        response = {};
        if (request['command'] == 'show_checkboxes') { //SHOW CKECKBOXES
            $('input.boberro_steamscreenshots_checkbox').show();
            response['status'] = 'ok';
        } else if (request['command'] == 'hide_checkboxes') { //HIDE CHECKBOXES
            $('input.boberro_steamscreenshots_checkbox').hide().attr('checked', false);
            response['status'] = 'ok';
        } else if (request['command'] == 'uncheck_all') { //UNCHECK ALL
            $('input.boberro_steamscreenshots_checkbox').attr('checked', false);
            response['status'] = 'ok';
        } else if (request['command'] == 'gather_data') { //GATHER DATA
            var data = gatherData();
            response['status'] = 'ok';
            response['data'] = data;
        } else {
            response['status'] = 'wrong command';
        }
        sendResponse(response);
    }
);

// init

var batchScreenshotManagement = document.getElementById("BatchScreenshotManagement");

insertNewCheckboxes();
checkNumberOfInfiniteElements();